const config = {
  // The live site address
  site: "https://jobs.rossstores.com",
  // Title for the HTML generated report
  title: "Ross Stores",
  // Path should be relative to this project folder
  localActivate: "../../../../source/repos/activate/",
  // Path should be relative to this project folder
  tenantShortName: "RossStores",
};

module.exports = config;