const fs = require("fs");
const webdriver = require("selenium-webdriver");
const { createHtmlReport } = require("axe-html-reporter");
const config = require("./tenantConfig.js");
const open = require("open");

async function generateA11yReport(
  generatedDriver,
  siteURL,
  pageName,
  projectTitle
) {
  await generatedDriver.get(siteURL);

  console.log(`Executing axe-core script for ${pageName}...`);
  const data = await fs.readFileSync(
    "./node_modules/axe-core/axe.min.js",
    "utf8"
  );
  await generatedDriver.executeScript(data.toString());
  let result = await generatedDriver.executeAsyncScript(`
    var callback = arguments[arguments.length - 1];
    axe.run({
      runOnly: ['wcag2a', 'wcag2aa', 'best-practices']
    }).then(results => callback(results))
  `);
  const reportHTML = createHtmlReport({
    results: result,
    options: {
      projectKey: projectTitle,
      doNotCreateReportFile: true,
    },
  });
  if (!fs.existsSync(`./reports/${config.tenantShortName}`)) {
    fs.mkdirSync(`./reports/${config.tenantShortName}`, { recursive: true });
  };
  await fs.writeFileSync(
    `./reports/${config.tenantShortName}/${pageName}-results.html`, reportHTML
  );
  console.log(`Results generated for ${pageName}, opening results in browser...`);
  await open(`./reports/${config.tenantShortName}/${pageName}-results.html`);
}

async function runAccessibilityTest() {
  const pages = fs.readdirSync(
    `${config.localActivate}Web/Views/${config.tenantShortName}/Creative/`
  );
  pages.unshift("/");
  pages.forEach(async (page) => {
    const path =
      page === "/"
        ? page
        : `/creative/${page.substring(0, page.lastIndexOf("."))}`;
    const pageTitle =
      page === "/" ? "Home" : page.substring(0, page.lastIndexOf("."));
    const url = `${config.site}${path}`;

    // Input capabilities
    const capabilities = {
      browserName: "chrome",
      browserVersion: "latest",
      os: "windows",
      os_version: "10",
      build: `Accessibility Test ${config.tenantShortName}`,
      name: `${config.title} - ${pageTitle}`,
    };
    let driver = new webdriver.Builder()
      .usingServer(
        "http://davidtorres_xYtBno:4YbT6tmfennZmCJDNjfV@hub-cloud.browserstack.com/wd/hub"
      )
      .withCapabilities(capabilities)
      .build();
    console.log(`Built BrowserStack Selenium driver for ${pageTitle}`);

    console.log(`Fetching ${url} using driver...`);
    await generateA11yReport(driver, url, pageTitle, config.title);
    await driver.quit();
  });
}

runAccessibilityTest().then(() => console.log("Test complete"));
